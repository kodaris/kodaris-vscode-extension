import { ExtensionContext, SecretStorage } from 'vscode';

export default class KAuthSettings {
	private static _instance: KAuthSettings;

	constructor(private secretStorage: SecretStorage) { }

	static init(context: ExtensionContext): void {
		KAuthSettings._instance = new KAuthSettings(context.secrets);
	}

	static get instance(): KAuthSettings {
		return KAuthSettings._instance;
	}

	async storeAuthEmail(username?: string): Promise<void> {
		if (username) {
			this.secretStorage.store('kodarisExt_username', username);
		}
	}

	async storeAuthPassword(password?: string): Promise<void> {
		if (password) {
			this.secretStorage.store('kodarisExt_password', password);
		}
	}

	async storeAccessToken(accessToken?: string): Promise<void> {
		if (accessToken) {
			this.secretStorage.store('kodarisExt_accessToken', accessToken);
		}
	}

	async getAuthEmail(): Promise<string | undefined> {
		return await this.secretStorage.get('kodarisExt_username');
	}

	async getAuthPassword(): Promise<string | undefined> {
		return await this.secretStorage.get('kodarisExt_password');
	}

	async getAccessToken(): Promise<string | undefined> {
		return await this.secretStorage.get('kodarisExt_accessToken');
	}
}
