/**
 * Kodaris HTTP Client.
 */
import KAuthSettings from './kAuthSettings';
import { Store } from 'tough-cookie';
import { HttpClient } from './http/httpClient';
import { HttpRequest } from './http/httpRequest';
import { OutgoingHttpHeaders } from 'http';

class KMClientError extends Error {
	error: any;
	name: string;

	constructor(message: string, error: any) {
		super(message);
		this.error = error;
		this.name = 'KMClientError';
	}
}

export class KMClient {
	private _settings: KAuthSettings;
	private _httpClient: HttpClient;

	private readonly cookieStore: Store;

	constructor() {
		this._settings = KAuthSettings.instance;
		this._httpClient = new HttpClient();
	}

	/**
	 * Takes a request and executes it on a kommerce server.
	 *
	 * Before making the request, we
	 *  1 - Turn on `withCredentials` in dev mode so our user session is maintained across CORS requests.
	 *  2 - Retrieve a csrf token and put it in the request headers. Required by kommerce servers. TODO
	 *
	 * After receiving the request response, we extract the data and return it. The app expects just the
	 * data back. No data wrapper objects that are specific to a kommerce server.
	 *
	 * FIXME - only set withCredentials if we are in dev mode.
	 *
	 * @param method Request method.
	 * @param url Request url.
	 * @param options Request options.
	 * @returns The response data.
	 */
	async request(method: string, url: string, options: OutgoingHttpHeaders, body?: any) {
		try {
			const req = new HttpRequest(method, url, options, body);
			const res = await this._httpClient.send(req);

			if (res.statusCode === 200) {
				try {
					const bodyJson = JSON.parse(res.body);
					if (bodyJson) {
						return typeof bodyJson.data !== 'undefined' ? bodyJson.data : null;
					}
				} catch (error) {
					if (typeof res.body === 'string') {
						return res.body;
					}
				}
			} else if (res.statusCode === 401) {
				console.log('logging in again...', res);
				// TODO: login again if unauthorized, save token and re-run request
				return this.handleError(res);
			} else {
				return this.handleError(res);
			}
		} catch (e) {
			return this.handleError(e);
		}
	}

	/**
	 * Invokes a `GET` request.
	 *
	 * @param url Request url.
	 * @param options Request options.
	 * @returns The data extracted from the response.
	 */
	async delete(url: string, options: OutgoingHttpHeaders, auth?: boolean) {
		if (auth) {
			const accessToken = await this._settings.getAccessToken();
			options = { ...options, 'Authorization': 'Bearer ' + accessToken };
		}
		return this.request('DELETE', url, options);
	}

	/**
	 * Invokes a `GET` request.
	 *
	 * @param url Request url.
	 * @param options Request options.
	 * @returns The data extracted from the response.
	 */
	async get(url: string, options: OutgoingHttpHeaders, auth?: boolean) {
		if (auth) {
			const accessToken = await this._settings.getAccessToken();
			options = { ...options, 'Authorization': 'Bearer ' + accessToken };
		}
		return this.request('GET', url, options);
	}

	/**
	 * Invokes a `PATCH` request.
	 *
	 * @param url Request url.
	 * @param body Request payload.
	 * @param options Request options.
	 * @returns The data extracted from the response.
	 */
	patch(url: string, options: OutgoingHttpHeaders, body?: any) {
		return this.request('PATCH', url, options, body);
	}

	/**
	 * Invokes a `POST` request.
	 *
	 * @param url Request url.
	 * @param body Request payload.
	 * @param options Request options.
	 * @returns The data extracted from the response.
	 */
	async post(url: string, options: OutgoingHttpHeaders, body?: any, auth?: boolean) {
		if (auth) {
			const accessToken = await this._settings.getAccessToken();
			options = { ...options, 'Authorization': 'Bearer ' + accessToken };
		}

		return this.request('POST', url, options, body);
	}

	/**
	 * Invokes a `PUT` request.
	 *
	 * @param url Request url.
	 * @param body Request payload.
	 * @param options Request options.
	 * @returns The data extracted from the response.
	 */
	put(url: string, options: OutgoingHttpHeaders, body?: any) {
		return this.request('PUT', url, options, body);
	}

	/**
	 * Handles an http error response. We display an error snackbar with
	 * the message.
	 *
	 * @param err The error to handle.
	 */
	handleError(err: any) {
		let message = 'Something went wrong';

		if (err.response) {
			const responseError = err.response || {};
			const {
				status,
				statusText,
				error
			} = responseError;

			if (status === 401) {
				message = 'Session expired.';
				// FIXME need a redirect to login
				// global.location.reload();
			} else if (error?.errors?.length) {
				message = error.errors[0];
			} else if (status && statusText) {
				message = `${status} - ${statusText}`;
			}
		} else if (err.message) {
			message = err.message;
		}

		throw new KMClientError(message, err);
	}
}
