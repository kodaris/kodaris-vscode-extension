/**
 * Extension's entry point. This is where the extension gets activated.
 */
import * as vscode from 'vscode';
import KAuthSettings from './kAuthSettings';

import { UserDataManager } from './http/userDataManager';
import { KFileSystemProvider } from './kFileSystemProvider';
import { KMSerice } from './kmService';

declare const navigator: unknown;

export async function activate(context: vscode.ExtensionContext) {
	KAuthSettings.init(context);
	await UserDataManager.initialize();

	const settings = KAuthSettings.instance;
	const kmService = new KMSerice();

	console.log('Activating Kodaris Editor');
	const kodFs = enableFs(context);

	const activationCommand = 'kod.commands.activateEditor';
	const commandHandler = async () => {
		console.log('Creating Kodaris Workspace');
		vscode.workspace.updateWorkspaceFolders(0, 0, { uri: vscode.Uri.parse('kod:/'), name: 'Kodaris Editor' });
	};

	context.subscriptions.push(vscode.commands.registerCommand(activationCommand, commandHandler));

	const loginCommand = 'kod.commands.login';
	const loginCommandHandler = async () => {
		const email = await settings.getAuthEmail();
		const password = await settings.getAuthPassword();

		if (!email || !password) {
			const loginPrompt = await vscode.window.showInputBox({ title: 'Enter Email', prompt: 'Kodaris Email Address' });
			await settings.storeAuthEmail(loginPrompt);

			const passwordPrompt = await vscode.window.showInputBox({ title: 'Enter Password', password: true, prompt: 'Kodaris Password' });
			await settings.storeAuthPassword(passwordPrompt);
		} else {
			vscode.window.showInformationMessage('You are already logged in to Kodaris.');
		}
	};
	context.subscriptions.push(vscode.commands.registerCommand(loginCommand, loginCommandHandler));

	context.subscriptions.push(vscode.commands.registerCommand('kod.commands.init', async _ => {
		const email = await settings.getAuthEmail();
		const password = await settings.getAuthPassword();

		if (email && password) {
			kmService.loginAndSaveToken().then(async () => {
				await kodFs.initRoot();
			}, () => {
				vscode.window.showErrorMessage('Please try again.');
			});
		} else {
			vscode.window.showErrorMessage('Please login to Kodaris first.');
		}
	}));

	context.subscriptions.push(vscode.commands.registerCommand('kod.commands.openSettings', _ => {
		vscode.commands.executeCommand('workbench.action.openWorkspaceSettings');
		vscode.commands.executeCommand('workbench.action.openSettings', 'kodaris');
	}));

	vscode.workspace.onDidCreateFiles(async (e) => {
		for (const file of e.files) {
			await kmService.createOrUpdateFile(file.path, undefined).catch(() => {
				vscode.window.showErrorMessage(`Failed creating file on the server with path: ${file.path}`);
			});
		}
	});

	vscode.workspace.onDidSaveTextDocument(async e => {
		const checkIfNotWorkspaceSettings = e.uri.path.split('.').pop() === 'code-workspace';
		if (!checkIfNotWorkspaceSettings) {
			await kmService.createOrUpdateFile(e.uri.path, e.getText()).catch(() => {
				vscode.window.showErrorMessage(`Failed saving file on the server with path: ${e.uri.path}`);
			});
		}
	});
}

function enableFs(context: vscode.ExtensionContext): KFileSystemProvider {
	const kodarisFS = new KFileSystemProvider();
	context.subscriptions.push(kodarisFS);

	return kodarisFS;
}
