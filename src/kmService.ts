/**
 * Kodaris Service. Connects to the HTTP Client.
 */
import KAuthSettings from './kAuthSettings';
import { KMClient } from './kmClient';

import { FormDataEncoder } from 'form-data-encoder';
import File from 'fetch-blob/file.js';
import { FormData } from 'formdata-polyfill/esm.min.js';
import { Readable } from 'stream';

export class KMSerice {
	private _kmClient: KMClient;
	private _settings: KAuthSettings;

	constructor() {
		this._kmClient = new KMClient();
		this._settings = KAuthSettings.instance;
	}

	/**
	 * Gets token for login authentication.
	 * @returns `token`
	 */
	token() {
		return this._kmClient.get('/api/user/employee/authToken', {});
	}

	/**
	 * Logins user with provided credentials.
	 * @param token string token
	 * @param email user email address
	 * @param pass user password
	 * @returns `access_token`
	 */
	login(token: string, email: string, pass: string) {
		return this._kmClient.post(`/api/user/employee/login?userName=${email}&password=${pass}`, { 'X-CSRF-TOKEN': token });
	}

	/**
	 * Logins user and saves token to secure settings.
	 * @returns void
	 */
	loginAndSaveToken() {
		return new Promise((resolve, reject) => {
			this.token().then(async token => {
				const email = await this._settings.getAuthEmail();
				const password = await this._settings.getAuthPassword();

				if (email && password) {
					this.login(token, email, password).then(async accessToken => {
						this._settings.storeAccessToken(accessToken.access_token);
						resolve(1);
					}, () => reject(0));
				} else {
					reject(0);
				}
			}, () => reject(0));
		});
	}

	/**
	 * Creates file or update it provided with text data.
	 * @param fileName name of file to update or create
	 * @param text string text data to save
	 * @returns void
	 */
	createOrUpdateFile(fileName: string, text?: string) {
		const newFileFormData = new FormData();
		const file = new File(text ? [text] : [], fileName);

		newFileFormData.append('file', file);

		const encoder = new FormDataEncoder(newFileFormData);
		return this._kmClient.post('/api/system/script/server', encoder.headers, Readable.from(encoder.encode()), true);
	}

	/**
	 * Deletes file with path.
	 * @param filePath path of file
	 * @returns void
	 */
	deleteFile(filePath: string) {
		return this._kmClient.delete(`/api/system/script/server?filePath=${filePath}`, {}, true);
	}

	/**
	 * Renames file from the server.
	 * @param oldPath file to rename
	 * @param newPath new file name
	 * @returns void
	 */
	renameFile(oldPath: string, newPath: string) {
		return new Promise((resolve, reject) => {
			this.getFileWithPath(oldPath).then(text => {
				this.deleteFile(oldPath).then(() => {
					this.createOrUpdateFile(newPath, text).then(() => {
						resolve(1);
					}, () => reject(0));
				}, () => reject(0));
			}, () => reject(0));
		});
	}

	/**
	 * Gets files and folders from the server. Defaults `size` to `1000`.
	 * @returns void
	 */
	getFileList(size = 1000) {
		return this._kmClient.get(`/api/system/script/server/list?size=${size}`, {}, true);
	}

	/**
	 * Gets files and folders of a sub folder from the server. Defaults `size` to `1000`.
	 * @param subFolderPath sub folder to fetch files and folders from
	 * @returns void
	 */
	getFileListSubfolder(subFolderPath: string, size = 1000) {
		return this._kmClient.get(`/api/system/script/server/list?size=${size}&subFolderPath=${subFolderPath}`, {}, true);
	}

	/**
	 * Gets data of file with provided path.
	 * @param filePath path of file to fetch data
	 * @returns void
	 */
	getFileWithPath(filePath: string) {
		return this._kmClient.get(`/api/system/script/server/view?filePath=${filePath}`, {}, true);
	}
}
