/**
 * Kodaris File System Provider.
 */
import { TextEncoder } from 'util';
import {
	Disposable, Event, EventEmitter, FileChangeEvent, FileChangeType, FileStat,
	FileSystemError,
	FileSystemProvider, FileType, Uri, workspace
} from 'vscode';
import { KMSerice } from './kmService';

export class File implements FileStat {
	type: FileType;
	ctime: number;
	mtime: number;
	size: number;

	name: string;
	path?: string;
	data?: Uint8Array;

	constructor(public uri: Uri, name: string) {
		this.type = FileType.File;
		this.ctime = Date.now();
		this.mtime = Date.now();
		this.size = 0;
		this.name = name;
	}
}

export class Directory implements FileStat {
	type: FileType;
	ctime: number;
	mtime: number;
	size: number;

	name: string;
	entries: Map<string, File | Directory>;

	path?: string;

	constructor(public uri: Uri, name: string) {
		this.type = FileType.Directory;
		this.ctime = Date.now();
		this.mtime = Date.now();
		this.size = 0;
		this.name = name;
		this.entries = new Map();
	}
}

export type Entry = File | Directory;

const textEncoder = new TextEncoder();

export class KFileSystemProvider implements FileSystemProvider, Disposable {
	static scheme = 'kod';

	private _emitter = new EventEmitter<FileChangeEvent[]>();
	onDidChangeFile: Event<FileChangeEvent[]> = this._emitter.event;

	private _bufferedEvents: FileChangeEvent[] = [];
	private _fireSoonHandle?: any;

	private readonly disposable: Disposable;

	private _kmService: KMSerice;

	constructor() {
		this.disposable = Disposable.from(
			workspace.registerFileSystemProvider(KFileSystemProvider.scheme, this, { isCaseSensitive: true })
		);

		this._kmService = new KMSerice();
	}

	private ROOT = 'kod:';

	async initRoot() {
		this._kmService.getFileList().then(res => {
			for (const folder of res.folders) {
				this.createDirectory(Uri.parse(`${this.ROOT}/${folder}/`));
				this._lookupKodarisDirectory(folder);
			}

			for (const file of res.files) {
				this.writeFile(Uri.parse(`${this.ROOT}${file.path}`), new Uint8Array(0), { create: true, overwrite: true, path: file.path });
			}
		});
	}

	dispose() {
		this.disposable?.dispose();
	}

	root = new Directory(Uri.parse('kod:/'), '');

	watch(uri: Uri, options: { recursive: boolean; excludes: string[]; }): Disposable {
		return new Disposable(() => { });
	}

	stat(uri: Uri): FileStat | Thenable<FileStat> {
		return this._lookup(uri, false);
	}

	readDirectory(uri: Uri): [string, FileType][] | Thenable<[string, FileType][]> {
		const entry = this._lookupAsDirectory(uri, false);
		let result: [string, FileType][] = [];

		for (const [name, child] of entry.entries) {
			result.push([name, child.type]);
		}

		return result;
	}

	createDirectory(uri: Uri): void | Thenable<void> {
		let basename = this._basename(uri.path);
		let dirname = uri.with({ path: this._dirname(uri.path) });
		let parent = this._lookupAsDirectory(dirname, false);
		let entry = new Directory(uri, basename);

		parent.entries.set(entry.name, entry);
		parent.mtime = Date.now();
		parent.size += 1;
		this._fireSoon({ type: FileChangeType.Changed, uri: dirname }, { type: FileChangeType.Created, uri });
	}

	readFile(uri: Uri): Uint8Array | Thenable<Uint8Array> {
		const file = this._lookupAsFile(uri, false);
		const data = this._lookupKodarisFile(file.uri.path);
		if (data) {
			return data;
		}
		throw FileSystemError.FileNotFound();
	}

	writeFile(uri: Uri, content: Uint8Array, options: { create: boolean; overwrite: boolean; path?: string }): void | Thenable<void> {
		let basename = this._basename(uri.path);
		let parent = this._lookupParentDirectory(uri);
		let entry = parent.entries.get(basename);

		if (entry instanceof Directory) {
			throw FileSystemError.FileIsADirectory(uri);
		}

		if (!entry && !options.create) {
			throw FileSystemError.FileNotFound(uri);
		}

		if (entry && options.create && !options.overwrite) {
			throw FileSystemError.FileExists(uri);
		}

		if (!entry) {
			entry = new File(uri, basename);
			parent.entries.set(basename, entry);

			this._fireSoon({ type: FileChangeType.Created, uri });
		}

		entry.mtime = Date.now();
		entry.size = content.byteLength;
		entry.data = content;
		entry.path = options.path;

		this._fireSoon({ type: FileChangeType.Changed, uri });
	}

	delete(uri: Uri, options: { recursive: boolean; }): void | Thenable<void> {
		let dirname = uri.with({ path: this._dirname(uri.path) });
		let basename = this._basename(uri.path);
		let parent = this._lookupAsDirectory(dirname, false);
		if (!parent.entries.has(basename)) {
			throw FileSystemError.FileNotFound(uri);
		}
		parent.entries.delete(basename);
		parent.mtime = Date.now();
		parent.size -= 1;

		this._kmService.deleteFile(uri.path).then(() => {
			// deleted info
		});

		this._fireSoon({ type: FileChangeType.Changed, uri: dirname }, { uri, type: FileChangeType.Deleted });
	}

	rename(oldUri: Uri, newUri: Uri, options: { overwrite: boolean; }): void | Thenable<void> {
		if (!options.overwrite && this._lookup(newUri, true)) {
			throw FileSystemError.FileExists(newUri);
		}

		let entry = this._lookup(oldUri, false);
		let oldParent = this._lookupParentDirectory(oldUri);

		let newParent = this._lookupParentDirectory(newUri);
		let newName = this._basename(newUri.path);

		oldParent.entries.delete(entry.name);
		entry.name = newName;
		newParent.entries.set(newName, entry);

		this._kmService.renameFile(oldUri.path, newUri.path).then(() => {
			this._fireSoon(
				{ type: FileChangeType.Deleted, uri: oldUri },
				{ type: FileChangeType.Created, uri: newUri }
			);
		});
	}

	copy(source: Uri, destination: Uri, options: { overwrite: boolean }): void | Thenable<void> {
		
	}

	private _lookupKodarisFile(filePath: string | undefined): PromiseLike<Uint8Array> {
		return new Promise((resolve) => {
			if (filePath) {
				this._kmService.getFileWithPath(filePath).then((res: string) => {
					resolve(textEncoder.encode(res));
				});
			} else {
				resolve(new Uint8Array(0));
			}
		});
	}

	private _lookupKodarisDirectory(subFolderPath: string): void {
		this._kmService.getFileListSubfolder(subFolderPath).then(res => {
			for (const folder of res.folders) {
				this.createDirectory(Uri.parse(`${this.ROOT}/${subFolderPath}/${folder}`));
				this._lookupKodarisDirectory(`${subFolderPath}/${folder}`);
			}

			for (const file of res.files) {
				this.writeFile(Uri.parse(`${this.ROOT}${file.path}`), new Uint8Array(0), { create: true, overwrite: true, path: file.path });
			}
		});
	}

	private _fireSoon(...events: FileChangeEvent[]): void {
		this._bufferedEvents.push(...events);

		if (this._fireSoonHandle) {
			clearTimeout(this._fireSoonHandle);
		}

		this._fireSoonHandle = setTimeout(() => {
			this._emitter.fire(this._bufferedEvents);
			this._bufferedEvents.length = 0;
		}, 5);
	}

	private _lookup(uri: Uri, silent: false): Entry;
	private _lookup(uri: Uri, silent: boolean): Entry | undefined;
	private _lookup(uri: Uri, silent: boolean): Entry | undefined {
		let parts = uri.path.split('/');
		let entry: Entry = this.root;
		for (const part of parts) {
			if (!part) {
				continue;
			}
			let child: Entry | undefined;
			if (entry instanceof Directory) {
				child = entry.entries.get(part);
			}
			if (!child) {
				if (!silent) {
					throw FileSystemError.FileNotFound(uri);
				} else {
					return undefined;
				}
			}
			entry = child;
		}
		return entry;
	}

	private _lookupAsDirectory(uri: Uri, silent: boolean): Directory {
		let entry = this._lookup(uri, silent);
		if (entry instanceof Directory) {
			return entry;
		}
		throw FileSystemError.FileNotADirectory(uri);
	}

	private _lookupAsFile(uri: Uri, silent: boolean): File {
		let entry = this._lookup(uri, silent);
		if (entry instanceof File) {
			return entry;
		}
		throw FileSystemError.FileIsADirectory(uri);
	}

	private _lookupParentDirectory(uri: Uri): Directory {
		const dirname = uri.with({ path: this._dirname(uri.path) });
		return this._lookupAsDirectory(dirname, false);
	}

	private _basename(path: string): string {
		path = this._rtrim(path, '/');
		if (!path) {
			return '';
		}

		return path.substr(path.lastIndexOf('/') + 1);
	}

	private _dirname(path: string): string {
		path = this._rtrim(path, '/');
		if (!path) {
			return '/';
		}

		return path.substr(0, path.lastIndexOf('/'));
	}

	private _rtrim(haystack: string, needle: string): string {
		if (!haystack || !needle) {
			return haystack;
		}

		const needleLen = needle.length,
			haystackLen = haystack.length;

		if (needleLen === 0 || haystackLen === 0) {
			return haystack;
		}

		let offset = haystackLen,
			idx = -1;

		while (true) {
			idx = haystack.lastIndexOf(needle, offset - 1);
			if (idx === -1 || idx + needleLen !== offset) {
				break;
			}

			if (idx === 0) {
				return '';
			}

			offset = idx;
		}

		return haystack.substring(0, offset);
	}
}
