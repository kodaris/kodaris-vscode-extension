import * as fs from 'fs-extra';
import * as os from 'os';
import * as path from 'path';

const restClientDir = 'kodaris-client';
const rootPath = path.join(os.homedir(), `.${restClientDir}`);

function getCachePath(): string {
	if (fs.existsSync(rootPath)) {
		return rootPath;
	}

	if (process.env.XDG_CACHE_HOME !== undefined) {
		return path.join(process.env.XDG_CACHE_HOME, restClientDir);
	}

	return rootPath;
}

function getConfigPath(): string {
	if (fs.existsSync(rootPath)) {
		return rootPath;
	}

	if (process.env.XDG_CONFIG_HOME !== undefined) {
		return path.join(process.env.XDG_CONFIG_HOME, restClientDir);
	}

	return rootPath;
}

export class UserDataManager {
	private static readonly cachePath: string = getCachePath();
	private static readonly configPath: string = getConfigPath();

	public static get cookieFilePath() {
		return path.join(this.cachePath, 'cookie.json');
	}

	private static get historyFilePath() {
		return path.join(this.cachePath, 'history.json');
	}

	private static get environmentFilePath() {
		return path.join(this.configPath, 'environment.json');
	}

	private static get responseSaveFolderPath() {
		return path.join(this.cachePath, 'responses/raw');
	}

	private static get responseBodySaveFolderPath() {
		return path.join(this.cachePath, 'responses/body');
	}

	public static async initialize(): Promise<void> {
		await Promise.all([
			fs.ensureFile(this.historyFilePath),
			fs.ensureFile(this.cookieFilePath),
			fs.ensureFile(this.environmentFilePath),
			fs.ensureDir(this.responseSaveFolderPath),
			fs.ensureDir(this.responseBodySaveFolderPath)
		]);
	}

	public static getResponseSaveFilePath(fileName: string) {
		return path.join(this.responseSaveFolderPath, fileName);
	}

	public static getResponseBodySaveFilePath(fileName: string) {
		return path.join(this.responseBodySaveFolderPath, fileName);
	}
}
