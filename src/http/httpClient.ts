import * as vscode from 'vscode';
import * as iconv from 'iconv-lite';
import { Cookie, CookieJar, Store } from 'tough-cookie';
import { HttpRequest } from './httpRequest';
import { HttpResponse } from './httpResponse';
import { UserDataManager } from './userDataManager';

import got = require('got');
import { convertBufferToStream } from './streamUtility';
import { getHeader, hasHeader } from './utils';
import { MimeUtility } from './mimeUtility';
import { RequestHeaders, ResponseHeaders } from './base';

const encodeUrl = require('encodeurl');
const ckStore = require('tough-cookie-file-store-bugfix');

type SetCookieCallback = (err: Error | null, cookie: Cookie) => void;
type SetCookieCallbackWithoutOptions = (err: Error, cookie: Cookie) => void;
type GetCookieStringCallback = (err: Error | null, cookies: string) => void;

export class HttpClient {
	private readonly cookieStore: Store;

	constructor() {
		const cookieFilePath = UserDataManager.cookieFilePath;
		this.cookieStore = new ckStore(cookieFilePath) as Store;
	}

	async send(httpRequest: HttpRequest): Promise<HttpResponse> {
		const websiteConfig = vscode.workspace.getConfiguration().get('kod.config.website');
		const options = await this.prepareOptions(httpRequest);

		let bodySize = 0;
		let headersSize = 0;
		const requestUrl = encodeUrl(httpRequest.url);
		const request: got.GotPromise<Buffer> = got(`${websiteConfig}${requestUrl}`, options);
		httpRequest.setUnderlyingRequest(request);
		(request as any).on('response', res => {
			if (res.rawHeaders) {
				headersSize += res.rawHeaders.map(h => h.length).reduce((a, b) => a + b, 0);
				headersSize += (res.rawHeaders.length) / 2;
			}
			res.on('data', chunk => {
				bodySize += chunk.length;
			});
		});

		const response = await request;
		const contentType = response.headers['content-type'];
		let encoding: string | undefined;
		if (contentType) {
			encoding = MimeUtility.parse(contentType).charset;
		}

		if (!encoding) {
			encoding = 'utf8';
		}

		const bodyBuffer = response.body;
		let bodyString = iconv.encodingExists(encoding) ? iconv.decode(bodyBuffer, encoding) : bodyBuffer.toString();

		// adjust response header case, due to the response headers in nodejs http module is in lowercase
		const responseHeaders: ResponseHeaders = HttpClient.normalizeHeaderNames(response.headers, response.rawHeaders);

		const requestBody = options.body;

		return new HttpResponse(
			response.statusCode,
			response.statusMessage,
			response.httpVersion,
			responseHeaders,
			bodyString,
			bodySize,
			headersSize,
			bodyBuffer,
			response.timings.phases,
			new HttpRequest(
				options.method!,
				requestUrl,
				HttpClient.normalizeHeaderNames(
					(response as any).request.gotOptions.headers as RequestHeaders,
					Object.keys(httpRequest.headers)),
				Buffer.isBuffer(requestBody) ? convertBufferToStream(requestBody) : requestBody,
				httpRequest.rawBody,
				httpRequest.name
			));
	}

	private async prepareOptions(httpRequest: HttpRequest): Promise<got.GotBodyOptions<null>> {
		const clonedHeaders = Object.assign({}, httpRequest.headers);
		const options: got.GotBodyOptions<null> = {
			headers: clonedHeaders,
			method: httpRequest.method,
			body: httpRequest.body,
			encoding: null,
			decompress: true,
			followRedirect: false,
			rejectUnauthorized: false,
			throwHttpErrors: false,
			cookieJar: new CookieJar(this.cookieStore, { rejectPublicSuffixes: false }),
			retry: 0,
			hooks: {
				afterResponse: [],
				// Following port reset on redirect can be removed after upgrade got to version 10.0
				// https://github.com/sindresorhus/got/issues/719
				beforeRedirect: [
					opts => {
						const redirectHost = ((opts as any).href as string).split('/')[2];
						if (!redirectHost.includes(':')) {
							delete opts.port;
						}
					}
				],
				beforeRequest: [],
			}
		};

		if (options.cookieJar) {
			const { getCookieString: originalGetCookieString, setCookie: originalSetCookie } = options.cookieJar;

			function _setCookie(cookieOrString: Cookie | string, currentUrl: string, opts: CookieJar.SetCookieOptions, cb: SetCookieCallback): void;
			function _setCookie(cookieOrString: Cookie | string, currentUrl: string, cb: SetCookieCallbackWithoutOptions): void;
			function _setCookie(cookieOrString: Cookie | string, currentUrl: string, opts: CookieJar.SetCookieOptions | SetCookieCallbackWithoutOptions, cb?: SetCookieCallback): void {
				if (opts instanceof Function) {
					cb = opts;
					opts = {};
				}
				opts.ignoreError = true;
				originalSetCookie.call(options.cookieJar, cookieOrString, currentUrl, opts, cb!);
			}
			//@ts-ignore
			options.cookieJar.setCookie = _setCookie;

			if (hasHeader(options.headers!, 'cookie')) {
				let count = 0;

				function _getCookieString(currentUrl: string, opts: CookieJar.GetCookiesOptions, cb: GetCookieStringCallback): void;
				function _getCookieString(currentUrl: string, cb: GetCookieStringCallback): void;
				function _getCookieString(currentUrl: string, opts: CookieJar.GetCookiesOptions | GetCookieStringCallback, cb?: GetCookieStringCallback): void {
					if (opts instanceof Function) {
						cb = opts;
						opts = {};
					}

					originalGetCookieString.call(options.cookieJar, currentUrl, opts, (err, cookies) => {
						if (err || count > 0 || !cookies) {
							cb!(err, cookies);
						}

						count++;
						cb!(null, [cookies, getHeader(options.headers!, 'cookie')].filter(Boolean).join('; '));
					});
				}
				//@ts-ignore
				options.cookieJar.getCookieString = _getCookieString;
			}
		}

		return options;
	}

	private static normalizeHeaderNames<T extends RequestHeaders | ResponseHeaders>(headers: T, rawHeaders: string[]): T {
		const headersDic: { [key: string]: string } = rawHeaders.reduce(
			(prev, cur) => {
				if (!(cur.toLowerCase() in prev)) {
					prev[cur.toLowerCase()] = cur;
				}
				return prev;
			}, {});
		const adjustedResponseHeaders = {} as RequestHeaders | ResponseHeaders;
		for (const header in headers) {
			const adjustedHeaderName = headersDic[header] || header;
			adjustedResponseHeaders[adjustedHeaderName] = headers[header];
		}

		return adjustedResponseHeaders as T;
	}
}
