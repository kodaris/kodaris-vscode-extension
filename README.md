# Kodaris Editor

This extension enables users to access Kodaris files - create, edit and delete from the server.

## Usage

* In an empty window.
* `F1` and select `Activate Kodaris Workspace`. You can then save this workspace to skip this step for later usage.
* `F1` and select `Login to Kodaris`. Enter credentials.
* `F1` again and select `Start Kodaris Editor`.

You can also activate another Kodaris Workspace and edit another website by configuring a new `Website` setting: 

Just press `F1` and select `Open Kodaris Editor Settings`.