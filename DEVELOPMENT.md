## Building

```bash
$> cd kodaris-vscode
$> npm install
```

## Packaging and Installation

```bash
$> npm install -g vsce
$> vsce package
$> code --install-extension kodaris-extension-1.0.0.vsix
```

Reload Visual Studio Code or open another window to start.

***TO BE FIXED: If it doesn't start after a second or two, just repeat the last step.***

### WHAT'S DONE:

* Login and authentication.
* Load files and build tree.
* Reading, Creating, Saving, Renaming and Deleting of files and folders.

### TO DO:

* ~~Ability to rename~~.
* ~~Add .vscodeignore~~
* Check copy and paste of files and folders.
* ~~Some safety checks.~~
* Auto reauthentication when a request returns 401.
* ~~Organize VSCode Kodaris commands~~.
